import mfrc522
from machine import Pin
import binascii
import struct

import time
#from machine import SoftSPI
from machine import SPI
from machine import I2C
from mp_i2c_lcd1602 import LCD1602

from collections import namedtuple

RSTPIN = 2
CSPIN = 15
SCKPIN = 14
MOSIPIN = 13
MISOPIN = 12
SCKFREQ = 400000
I2CSCLPIN = 5
I2CSDAPIN = 4
LEDPWRPIN = 9
LEDRDRPIN = 16
BUTTONPIN = 10

LCD_I2C_ADDR = 39 #0x27
RTC_I2C_ADDR = None

lcd_i2c = None
reader = None
LCD = None
ledpwr = None
ledrdr = None
button = None

BTNHI = 1
BTNLO = 0

LEDOFF = 0
LEDON = 1
LEDSLOWBLINK = 2
LEDFASTBLINK = 3

READEROFF = 0
READERON = 1
READERIDLE = 2
READERWAIT = 3

LCDOFF = 0
LCDON = 1
LCDBOOTMSG = 2
LCDWAIT = 3
LCDSCAN = 4
LCDSHOW = 5
LCDPERSISTSHOW = 6
LCDERROR = 7

ledpwr_state = 0
prev_ledpwr_state = 0
ledrdr_state = 0
prev_ledrdr_state = 0
button_state = 0
prev_button_state = 0
reader_state = 0
prev_reader_state = 0
lcd_state = 0
prev_lcd_state = 0

tag_id = None
persist_time = 0
ledpwr_change = 0
ledrdr_change = 0

User = namedtuple("User", ("uid", "tag_type", "username"))
Users=[User(0x459ef68d, 0x10, "Pierre-Yves"), User(0x2c19f38c, 0x10, "Xavier"), User(0x5d9f324b, 0x10, "User3")]

def check_spi(rstpin, cspin):
        led = Pin(LEDPWRPIN, Pin.OUT)
        led.value(1)
        time.sleep_ms(1000)
        led.value(0)
        time.sleep_ms(1000)
        led.value(1)
        time.sleep_ms(1000)
        led.value(0)

        rst = Pin(rstpin, Pin.OUT)
        cs = Pin(cspin, Pin.OUT)

        #spi = SoftSPI(baudrate=SCKFREQ, polarity=0, phase=0, firstbit=SoftSPI.MSB, sck=Pin(SCKPIN), mosi=Pin(MOSIPIN), miso=Pin(MISOPIN))
        #spi.init(baudrate=SCKFREQ) # set the baudrate
        spi = SPI(1, baudrate=SCKFREQ, polarity=0, phase=0)

        rst.value(0)
        print("asserting rst pin")
        time.sleep_ms(2000)

        cs.value(1)
        rst.value(1)
        print("deasserting rst pin")
        time.sleep_ms(200)

        reg = 0x37 << 1 | 0x80
        #reg = 0x0D << 1 | 0x80
        print("reading register at address @%x" % reg)
        cs.value(0)
        time.sleep_ms(200)
        spi.write(bytearray([reg]))
        val = spi.read(1)
        cs.value(1)

        print(val)


def do_read():

    #reader = mfrc522.MFRC522(sck=GPIO14(D5), mosi=GPIO13(D7), miso=GPIO12(D6), rst=GPIO2(D4), cs=GPIO15(D8))
    reader = mfrc522.MFRC522(0, 0, 0, RSTPIN, CSPIN)

    reg = 0x37 << 1 | 0x80
    #reg = 0x0D << 1 | 0x80
    print("reading register at address @%x" % reg)
    val = reader._rreg(0x37)
    print("version: %x" % val)

    print("")
    print("Place card before reader to read from address 0x08")
    print("")

    try:
        while True:

            (stat, tag_type) = reader.request(reader.REQIDL)

            if stat == reader.OK:

                (stat, raw_uid) = reader.anticoll()

                if stat == reader.OK:
                    print("New card detected")
                    print("  - tag type: 0x%02x" % tag_type)
                    print("  - uid	 : 0x%02x%02x%02x%02x" % (raw_uid[0], raw_uid[1], raw_uid[2], raw_uid[3]))
                    print("")

                    if reader.select_tag(raw_uid) == reader.OK:

                        key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]

                        if reader.auth(reader.AUTHENT1A, 8, key, raw_uid) == reader.OK:
                            print("Address 8 data: %s" % reader.read(8))
                            reader.stop_crypto1()
                        else:
                            print("Authentication error")
                    else:
                        print("Failed to select tag")

    except KeyboardInterrupt:
        print("Bye")

def ledpwr_fsm():
    global ledpwr
    global ledpwr_state, prev_ledpwr_state
    global ledpwr_change

    if ledpwr_state == LEDOFF:
        ledpwr.value(0)
        prev_ledpwr_state = ledpwr_state
    if ledpwr_state == LEDON:
        ledpwr.value(1)
        prev_ledpwr_state = ledpwr_state
    if ledpwr_state == LEDSLOWBLINK:
        if ledpwr_state != prev_ledpwr_state:
            ledpwr_change = time.ticks_add(time.ticks_ms(), 1000)
        prev_ledpwr_state = ledpwr_state
        if time.ticks_diff(ledpwr_change, time.ticks_ms()) < 0:
            ledpwr.value(not ledpwr.value())
    if ledpwr_state == LEDFASTBLINK:
        if ledpwr_state != prev_ledpwr_state:
            ledpwr_change = time.ticks_add(time.ticks_ms(), 300)
        prev_ledpwr_state = ledpwr_state
        if time.ticks_diff(ledpwr_change, time.ticks_ms()) < 0:
            ledpwr.value(not ledpwr.value())

def ledrdr_fsm():
    global ledrdr
    global ledrdr_state, prev_ledrdr_state
    global ledrdr_change

    if ledrdr_state == LEDOFF:
        ledrdr.value(0)
        prev_ledrdr_state = ledrdr_state
    if ledrdr_state == LEDON:
        ledrdr.value(1)
        prev_ledrdr_state = ledrdr_state
    if ledrdr_state == LEDSLOWBLINK:
        prev_ledrdr_state = ledrdr_state
        if time.ticks_diff(ledrdr_change, time.ticks_ms()) < 0:
            ledrdr_change = time.ticks_add(time.ticks_ms(), 1000)
            ledrdr.value(not ledrdr.value())
    if ledrdr_state == LEDFASTBLINK:
        if time.ticks_diff(ledrdr_change, time.ticks_ms()) < 0:
            ledrdr_change = time.ticks_add(time.ticks_ms(), 30)
            ledrdr.value(not ledrdr.value())

def rfid_read_once(reader):
    (stat, tag_type) = reader.request(reader.REQIDL)
    if stat == reader.OK:
        (stat, raw_uid) = reader.anticoll()

        if stat == reader.OK:
            print("New card detected")
            print("  - tag type: 0x%02x" % tag_type)
            print("  - uid	 : 0x%02x%02x%02x%02x" % (raw_uid[0], raw_uid[1], raw_uid[2], raw_uid[3]))
            print("")

            if reader.select_tag(raw_uid) == reader.OK:
                key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]
                if reader.auth(reader.AUTHENT1A, 8, key, raw_uid) == reader.OK:
                    print("Address 8 data: %s" % reader.read(8))
                    reader.stop_crypto1()
                else:
                    print("Authentication error")
            else:
                print("Failed to select tag")
        else:
            return -1
    else:
        return -1

    return struct.unpack(">l", bytearray(raw_uid[:4]))[0]

def init():
    global lcd_i2c, reader, LCD
    global ledpwr, ledrdr, button
    global ledpwr_state, ledrdr_state
    global reader_state, prev_reader_state
    global lcd_state, prev_lcd_state
    global button_state, prev_button_state

    ledpwr = Pin(LEDPWRPIN, Pin.OUT)
    ledpwr_state = LEDFASTBLINK
    ledrdr = Pin(LEDRDRPIN, Pin.OUT)
    ledrdr_state = LEDOFF
    button = Pin(BUTTONPIN, Pin.IN)
    lcd_i2c = I2C(sda=Pin(I2CSDAPIN), scl=Pin(I2CSCLPIN))
    reader = mfrc522.MFRC522(0, 0, 0, RSTPIN, CSPIN)
    LCD = LCD1602(lcd_i2c, addr=LCD_I2C_ADDR)

    LCD.backlight(1)
    lcd_state = LCDBOOTMSG;
    prev_lcd_state = LCDBOOTMSG;

    prev_button_state = button_state = button.value()
    reader_state = READERIDLE
    prev_reader_state = READERIDLE

def read_input():
    global button
    global button_state

    if button.value() == BTNHI:
        if button_state != button.value():
            print("Button High")
        button_state = BTNHI
    if button.value() == BTNLO:
        if button_state != button.value():
            print("Button Low")
        button_state = BTNLO

def button_changed_state():
    global prev_button_state, button_state

    ret = False
    if prev_button_state != button_state:
        ret = True
    prev_button_state = button_state
    return ret

def reader_event():
    global tag_id, reader, reader_state, prev_reader_state, lcd_state
    if reader_state == READEROFF:
        if reader_state != prev_reader_state:
            print("reader off")
        prev_reader_state = reader_state
    if reader_state == READERON:
        if reader_state != prev_reader_state:
            print("reader on")
        prev_reader_state = reader_state
    if reader_state == READERIDLE:
        if reader_state != prev_reader_state:
            print("reader idle")
        prev_reader_state = reader_state
    if reader_state == READERWAIT:
        new_tag = rfid_read_once(reader)
        if new_tag != -1:
            tag_id = new_tag
            lcd_state = LCDSHOW
        prev_reader_state = reader_state

def lcd_event():
    global LCD, lcd_state, prev_lcd_state, reader_state
    global tag_id
    global persist_time
    global ledpwr_state, prev_ledpwr_state
    global ledrdr_state, prev_ledrdr_state

    if lcd_state == LCDOFF:
        if(prev_lcd_state != lcd_state):
            print("LCD off")
        LCD.backlight(0)
        prev_lcd_state = LCDOFF
    if lcd_state == LCDON:
        if(prev_lcd_state != lcd_state):
            print("LCD on")
        prev_lcd_state = LCDON
        LCD.backlight(1)
    if lcd_state == LCDBOOTMSG:
        LCD.clear()
        LCD.puts("RFID counter")
        print("RFID counter")
        ledpwr_state = LEDON
        ledrdr_state = LEDOFF
        prev_lcd_state = LCDBOOTMSG
        time.sleep_ms(2000)
        lcd_state = LCDWAIT
    if lcd_state == LCDWAIT:
        if(prev_lcd_state != lcd_state):
            LCD.clear()
            LCD.puts("Press button to", 0, 0)
            LCD.puts("scan new tag", 0, 1)
            print("press button")
            ledrdr_state = LEDOFF
        if button_changed_state():
            lcd_state = LCDSCAN
        else:
            prev_lcd_state = LCDWAIT
    if lcd_state == LCDSCAN:
        if(prev_lcd_state != lcd_state):
            LCD.clear()
            LCD.puts("Awaiting tag", 0, 1)
            ledrdr_state = LEDFASTBLINK
        reader_state = READERWAIT
        prev_lcd_state = LCDSCAN
    if lcd_state == LCDSHOW:
        if(prev_lcd_state != lcd_state):
            print("show tag")
            ledrdr_state = LEDON
        prev_lcd_state = lcd_state
        reader_state = READERIDLE
        LCD.clear()
        txt_to_show="Unknown tag"
        for u in Users:
            print("user.username=%s user.uid=%d tag_id=%s" % (u.username, u.uid, tag_id))
            if u.uid==tag_id :
                txt_to_show=u.username
        print("####")
        LCD.puts(txt_to_show, 0, 1) 
        prev_lcd_state = LCDSHOW
        persist_time = 0
        lcd_state = LCDPERSISTSHOW
    if lcd_state == LCDPERSISTSHOW:
        if(prev_lcd_state != lcd_state):
            print("persist show user")
            persist_time = time.ticks_add(time.ticks_ms(), 2000)
        prev_lcd_state = lcd_state

        if time.ticks_diff(persist_time, time.ticks_ms()) < 0:
            lcd_state = LCDWAIT
    if lcd_state == LCDERROR:
        if(prev_lcd_state != lcd_state):
            print("No tag detected")
            LCD.clear()
            LCD.puts("No tag detected", 0,1)
        prev_lcd_state = LCDERROR



def main_loop():

    while True:
        read_input()
        ledpwr_fsm()
        ledrdr_fsm()
        reader_event()
        lcd_event()

print("RFID counter app start")
init()
main_loop()
