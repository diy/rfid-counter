# **RFID counter** #

### A simple RFID persistent counter

This project purpose is to manage persistent counters that are incremented when you scan a RFID tag.
It is written in micropython for nodemcu v3 platforms.

The idea came from my need to count the times I would ride my bike to work.
Until now, I am managing an excel sheet but thought it would be nice if I could
only have to scan a RFID tag each day I ride my bike to work and besides it could
be used by colleagues as well.

### HW parts required

Of course for this project, you will need to own a nodemcu like mine (nodemcu v3 - esp8266) or adpat it to your own micropython platform.

This project is based on some rfid reader. The selected one is a mfrc522 reader reference platform from AZ-delivery.
A LCD1602 (16 chars and 2 lanes) is also used in conjunctin with an I2C IO expander (PCF8574T) to show/interact with the user.

Evolution:
Counters are timestamped via a I2C RTC clock 32kHz (DS3231) and counters are stored to a 32kBytes I2C flash (AT24C32)
You can use a combined module like the ZS042 which contains a RTC clock and a flash memory.
For this project a few leds will be needed (and thus resistors) and some push button connected to GPIO pins.

## Software dependencies

The nodemcu is running micropyhton for this project (not LUA). See this link for instructions on how to programm the micropython firmware to your nodemcu
[Instructions](https://micropython instructions)

MFRC522 rfid micropyhton library: [MFRC522](https://github.com/wendlers/micropython-mfrc522.git)
LCD1602/PCF8574T micropython library is taken from mpy-lib: [mpy-lib](https://github.com/micropython-Chinese-Community/mpy-lib.git)


## Status
Today, I can read and identify pre-registered (hard coded users). Leds, button and 2x16 lcd are working
pretty much properly.
Electronics is mounted on a breadboard at the moment.


## TODO
SW:
- manage RTC in order to timestamp the counter events
- store users data in ROM
- store event logs in a journal
- setup a webserver so that the data can be fetched and used externally
- setup admin interface to webserver to manage user and other admin stuffs not yet defined

PCB:
- design 1 layer pcb

3D:
- design case
